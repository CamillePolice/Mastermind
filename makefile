# Link
#
OBJ=./obj
BIN=./bin
SRC=./src
CFLAGS += -ggdb
HFILES=-I./include

all: $(BIN)/main

$(BIN)/main:	$(OBJ)/main.o $(OBJ)/game.o
	gcc -o $@ $^

obj/game.o: $(SRC)/game.c
	gcc -o $(OBJ)/game.o $(HFILES) -c $(SRC)/game.c

obj/main.o: $(SRC)/main.c
	gcc -o $(OBJ)/main.o $(HFILES) -c $(SRC)/main.c

clean :
# To be completed
	rm -rf obj/*.o bin/*

#
# Test the functionality
test :

# To be completed
