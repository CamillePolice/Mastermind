#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "game.h"

#define WIDTH 8
#define LENGTH 15
#define varTEST 0

int game();
int rules();

void gameMenu();
void newCode(char *secretCode);
void endOfTheGameWin();
void endOfTheGameLoose();
void displayCode(char tab[]);
void exitGame();

int choice = 0;
char mastermindGrid[LENGTH][WIDTH];

int game() {
    int globalTry = 0, nbLetters = 0;
    char secretCode[4], attempt[5] = {'\0'};
    char letter;
    char mastermindGrid[LENGTH][WIDTH] =  {
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'},
        {'_','_','_','_','F','F','F','F'}
    };

    newCode(secretCode);
    system("clear");

    while( globalTry <= 15) {
        printf("You can choose between these colors :\n");
        printf("Green(g) - Red(r) - Blue(b)\n");
        printf("Pink(p) - Orange(o) - White(w)\n\n");
        printf("F = bad position but good color\n");
        printf("T = The color is good and well placed\n");
        printf("B = The color is in the code, but the rank isn't good\n");

        // Display the grid
      for(int i=0; i<LENGTH;i++) {
        printf("[%3d] -> ", i+1);
        for(int j=0; j<WIDTH;j++) {
          printf("%c | ", mastermindGrid[i][j]);
          if(j==3)
            printf(" | ");
        }
        printf("\n");
      }

      // Ask the appempt of the player
      while(nbLetters != 4 ){
          printf("\nTry %d -> \n", globalTry+1);
          printf("Give the 4 letters of your combinaison -> \n");
          scanf("%s", &attempt);
          fflush(stdin);
          for(int i=0;i<4;i++) {
              if(attempt[nbLetters] == 'g' || attempt[nbLetters] == 'r' || attempt[nbLetters] == 'b' || attempt[nbLetters] == 'p' || attempt[nbLetters] == 'o' || attempt[nbLetters] == 'w')
                    nbLetters++;
              else {
                    printf("Wrong letter, please, give your 4 colors.\n");
                    i=4;
              }
          }
      }
      displayCode(attempt);


    }

    // All fine
    return 0;
    }

int rules() {
    system("clear");
    printf("********** MASTERMIND's RULES **********\n");

    printf("\nThe program randomly chooses a combination of 4 random color pawns\n" );
    printf("among the 6 colors available.\n" );
    printf("Green (g)\n Red (r)\n Blue (b)\n Pink (p)\n Orange (o)\n White (w)\n");
    printf("The combinaison chose by the program can have many times the same color.\n" );
    printf("You must find the good combinaison. Each attempt where, you'll have\n" );
    printf("some pawns in the combinaison will reveal to you either :\n" );
    printf("a present but bad placed pawn (red pawn)\n" );
    printf("a well placed pawn (green pawn)\n" );
    printf("You have 15 attemps to find the good one !\n\n" );
    printf("Good luck hackers !\n\n");
    printf("Enter (1) to play a game, and (2) to come back at the main menu.\n" );
    scanf("%d", &choice);
    printf("%d", choice);
    printf("Test Commit");

    switch(choice) {
      case 1:
          game();
          exitGame();
          break;
      case 2:
          gameMenu();
          break;
      default:
          printf("Wrong choice, please");
          break;
      }
    // All fine
    return 0;
}


void newCode(char *secretCode) {
  // Get a random sequence each time
  for(int i=0; i<4;i++) {

    switch(rand() % 6) {
      case 0:
        secretCode[i] = 'g';
        break;
      case 1:
        secretCode[i] = 'r';
        break;
      case 2:
        secretCode[i] = 'b';
        break;
      case 3:
        secretCode[i] = 'p';
        break;
      case 4:
        secretCode[i] = 'o';
        break;
      case 5:
        secretCode[i] = 'w';
        break;
    }
  }
}

void displayCode(char *tab) {
  for(int i=0; i<4;i++) {
    printf("tab %d => %c\n", i, tab[i]);
  }
}

void gameMenu() {
    system("clear"); // clear the console

    printf("********** MENU OF THE MASTERMIND **********\n");
    printf("To choose your choice, enter the number of the followings possibilities :\n");
    printf("\n");
    printf("1 -> Play\n");
    printf("2 -> Rules\n");
    printf("3 -> Quit\n");
    printf("\n");
    printf("Take your choice :\n");
}

void endOfTheGameWin() {
  printf("The game is ended\n");
  printf("Congratulations, Hacker. You win\n");
  printf("Another game ? Enter -> (0) or main menu ? Enter -> (1)\n");
}

void endOfTheGameLoose() {
  printf("The game is ended\n");
  printf("You loose, you couldn't break this code !\n");
  printf("Maybe try again ? Enter -> (0) or stay in your deception. Enter -> (1)\n");
}

void exitGame() {
    printf("Thanks to play. See you soon !\n");
    exit(-1);
}
