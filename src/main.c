#include <stdio.h>
#include <stdlib.h>

#include "game.h"

int main() {

  int choice = 0;

  while(1) {
      gameMenu();
      scanf("%d", &choice);
      switch(choice) {
        case 1:
            game();
            exitGame();
            break;
        case 2:
            rules();
            break;
        case 3:
            exitGame();
            break;
        default:
            printf("Wrong choice, please");
            break;
        }
    }
    return 0;
}
